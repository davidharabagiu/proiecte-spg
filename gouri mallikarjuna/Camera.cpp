#include "Camera.hpp"

namespace gps
{
	Camera::Camera(glm::vec3 cameraPosition)
	{
		this->cameraPosition = cameraPosition;
		this->xRotation = 0;
		this->yRotation = 0;
		updateCameraTarget();
	}

	Camera::Camera(glm::vec3 cameraPosition, float xRotation, float yRotation)
	{
		this->cameraPosition = cameraPosition;
		this->xRotation = xRotation;
		this->yRotation = yRotation;
		updateCameraTarget();
	}

	glm::mat4 Camera::getViewMatrix()
	{
		static glm::vec3 up(0.0f, 1.0f, 0.0f);
		return glm::lookAt(cameraPosition, cameraTarget, up);
	}

	void Camera::move(MOVE_DIRECTION direction, float speed)
	{
		glm::vec3 newCameraPosition = cameraPosition;
		glm::vec3 newCameraTarget = cameraTarget;

		if (direction == MOVE_FORWARD)
		{
			newCameraPosition.z += glm::cos(yRotation) * speed;
			newCameraTarget.z += glm::cos(yRotation) * speed;
			newCameraPosition.x += glm::sin(yRotation) * speed;			
			newCameraTarget.x += glm::sin(yRotation) * speed;
		}
		else if (direction == MOVE_BACKWARD)
		{
			newCameraPosition.z -= glm::cos(yRotation) * speed;
			newCameraTarget.z -= glm::cos(yRotation) * speed;
			newCameraPosition.x -= glm::sin(yRotation) * speed;
			newCameraTarget.x -= glm::sin(yRotation) * speed;
		}
		else if (direction == MOVE_LEFT)
		{
			newCameraPosition.z -= glm::sin(yRotation) * speed;
			newCameraPosition.x += glm::cos(yRotation) * speed;
			newCameraTarget.z -= glm::sin(yRotation) * speed;
			newCameraTarget.x += glm::cos(yRotation) * speed;
		}
		else if (direction == MOVE_RIGHT)
		{
			newCameraPosition.z += glm::sin(yRotation) * speed;
			newCameraPosition.x -= glm::cos(yRotation) * speed;
			newCameraTarget.z += glm::sin(yRotation) * speed;
			newCameraTarget.x -= glm::cos(yRotation) * speed;
		}

		cameraPosition.z = newCameraPosition.z;
		cameraTarget.z = newCameraTarget.z;

		cameraPosition.x = newCameraPosition.x;
		cameraTarget.x = newCameraTarget.x;
	}

	void Camera::updateCameraTarget()
	{
		cameraTarget.x = cameraPosition.x + glm::sin(yRotation);
		cameraTarget.y = cameraPosition.y + glm::sin(xRotation);
		cameraTarget.z = cameraPosition.z + glm::cos(yRotation) * glm::cos(xRotation);
	}
	glm::vec3 Camera::getCameraTarget() 
	{
		return cameraTarget;
	}
	void Camera::rotate(float pitch, float yaw)
	{
		float newXRotation = xRotation + pitch;
		if (newXRotation <= MAX_X_ROTATION && newXRotation >= MIN_X_ROTATION)
		{
			xRotation = newXRotation;
		}

		yRotation = yRotation + yaw;

		updateCameraTarget();
	}

	glm::vec3 Camera::getCameraPosition()
	{
		return cameraPosition;
	}

	glm::vec3 Camera::getCameraDirection()
	{
		return glm::normalize(cameraTarget - cameraPosition);
	}
}