#pragma once

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "GLEW\glew.h"
#include "GLFW\glfw3.h"
#include "Shader.hpp"
#include "Model3D.hpp"

class Planet
{

public:

	Planet()
	{
	}

	Planet(
		gps::Model3D model,
		GLfloat size,
		GLfloat rotationSpeed,
		GLfloat resolutionSpeed,
		glm::vec3 origin,
		GLfloat distanceFromSun,
		GLfloat tilt
	);

	void Update(GLfloat deltaTime);
	void Draw(gps::Shader& shader, glm::mat4& viewMatrix);
	void Draw(gps::Shader& shader);

private:

	gps::Model3D model;
	GLfloat size;
	GLfloat tilt;
	GLfloat rotation;
	GLfloat rotationSpeed;
	GLfloat resolutionSpeed;
	glm::vec3 origin;
	GLfloat distanceFromSun;
	GLfloat resolutionProgress;
	glm::mat4 modelMatrix;

};