#include "Planet.hpp"

Planet::Planet(
	gps::Model3D model,
	GLfloat size,
	GLfloat rotationSpeed,
	GLfloat resolutionSpeed,
	glm::vec3 origin,
	GLfloat distanceFromSun,
	GLfloat tilt
) :
	model(model),
	size(size),
	rotation(0.0f),
	rotationSpeed(rotationSpeed),
	resolutionSpeed(resolutionSpeed),
	origin(origin),
	distanceFromSun(distanceFromSun),
	resolutionProgress(0.0f),
	tilt(tilt)
{
}

// this is called every frame

void Planet::Update(GLfloat deltaTime)
{
	rotation += rotationSpeed * deltaTime;

	if (distanceFromSun > 0.0f)
	{
		resolutionProgress += resolutionSpeed * deltaTime / distanceFromSun * 5000.0f;
	}

	GLfloat x = origin.x + glm::cos(resolutionProgress) * distanceFromSun;
	GLfloat y = origin.y;
	GLfloat z = origin.z + glm::sin(resolutionProgress) * distanceFromSun;

	modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));
	modelMatrix = glm::rotate(modelMatrix, tilt, glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, rotation, glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(size));
}

void Planet::Draw(gps::Shader& shader, glm::mat4& viewMatrix)
{
	shader.useShaderProgram();

	GLuint modelLoc = glGetUniformLocation(shader.shaderProgram, "model");
	GLuint normalMatrixLoc = glGetUniformLocation(shader.shaderProgram, "normalMatrix");

	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
	glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(viewMatrix * modelMatrix));

	model.Draw(shader);
}

void Planet::Draw(gps::Shader& shader)
{
	shader.useShaderProgram();

	GLuint modelLoc = glGetUniformLocation(shader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	model.Draw(shader);
}
