//
//  main.cpp
//  OpenGL Advances Lighting
//
//  Created by CGIS on 28/11/16.
//  Copyright � 2016 CGIS. All rights reserved.
//

#define GLEW_STATIC

#include <iostream>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include <cstdlib>
#include <ctime>
#include "Shader.hpp"
#include "Camera.hpp"
#define TINYOBJLOADER_IMPLEMENTATION

#include "Model3D.hpp"
#include "Mesh.hpp"
#include "Skybox.hpp"

int glWindowWidth = 1280;
int glWindowHeight = 720;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;

glm::mat4 model;
GLuint modelLoc;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;
glm::mat3 lightDirMatrix;
GLuint lightDirMatrixLoc;

glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;
glm::vec3 lightPos;
GLuint lightPosLoc;

GLuint isFog = 1;
GLuint isFogLoc;
GLuint isLight = 1;
GLuint isLightLoc;

GLfloat fov = 45.0f;

glm::vec3 cameraPosition(20.0f, 1.0f, 21.0f);

glm::vec3 treeOffsets[5][5];

gps::Camera myCamera(cameraPosition, 0.0f, 0.0f);
float cameraSpeed = 0.225f;

bool pressedKeys[1024];
float angle = 0.0f;
GLfloat lightAngle;

const int SHADOW_WIDTH = 4096, SHADOW_HEIGHT = 4096;
GLuint shadowMapFBO;
GLuint depthMapTexture;

gps::Model3D ground;
gps::Model3D objCasa;
gps::Model3D casa;
gps::Model3D usaDeschisa;
gps::Model3D usa;
gps::Model3D dovleac;
gps::Model3D copac;
gps::Model3D porc;
gps::SkyBox mySkybox;

gps::Shader myCustomShader;
gps::Shader depthMapShader;
gps::Shader skyboxShader;

glm::vec2 oldMousePos;

bool deschis = false;
bool r = false;
bool t = false;
bool usaDesc = false;


int sceneMode = 0;
bool firstMouse = true;

GLfloat pigDirection = 0.0f;
GLfloat pigX = 0.0f;
GLfloat pigY = 0.0f;
GLfloat pigZ = 0.0f;
GLfloat pigJumpProg = 0.0f;

GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(fov), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	static double lastxpos = -1;
	static double lastypos = -1;

	if (lastxpos == -1 || lastypos == -1)
	{
		lastxpos = xpos;
		lastypos = ypos;
		return;
	}

	myCamera.rotate((lastypos - ypos) / 250.0f, (lastxpos - xpos) / 250.0f);
	lastxpos = xpos;
	lastypos = ypos;
}

void processMovement()
{
	static GLfloat lastTimeStamp = glfwGetTime();
	GLfloat currentTimeStamp = glfwGetTime();
	GLfloat deltaTime = currentTimeStamp - lastTimeStamp;
	lastTimeStamp = currentTimeStamp;

	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_R]){
		fov += 1.0f;
		projection = glm::perspective(glm::radians(fov), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);

		myCustomShader.useShaderProgram();
		glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "projection"), 1, GL_FALSE,
			glm::value_ptr(projection));

		skyboxShader.useShaderProgram();
		glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,
			glm::value_ptr(projection));
	}
	if (pressedKeys[GLFW_KEY_T]){
		fov -= 1.0f;
		projection = glm::perspective(glm::radians(fov), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);

		myCustomShader.useShaderProgram();
		glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "projection"), 1, GL_FALSE,
			glm::value_ptr(projection));

		skyboxShader.useShaderProgram();
		glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,
			glm::value_ptr(projection));
	}
	if (pressedKeys[GLFW_KEY_O]){
		if (usaDesc == true){
			usaDesc = false;
		}
		else{
			usaDesc = true;
		}
		pressedKeys[GLFW_KEY_O] = false;
	}

	if (pressedKeys[GLFW_KEY_2]) {
		sceneMode += 1;
		if (sceneMode == 4) {
			sceneMode = 0;
		}
		switch (sceneMode){
		case 0:
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			break;
		case 1:
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;
		case 2:
			glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
			break;
		case 3:
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glEnable(GL_POLYGON_SMOOTH);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_DST_ALPHA);
			break;
		}
		pressedKeys[GLFW_KEY_2] = false;
	}

	if (pressedKeys[GLFW_KEY_F])
	{
		isFog = !isFog;
		glUniform1i(isFogLoc, isFog);
		pressedKeys[GLFW_KEY_F] = false;
	}

	if (pressedKeys[GLFW_KEY_L])
	{
		isLight = !isLight;
		glUniform1i(isLightLoc, isLight);
		pressedKeys[GLFW_KEY_L] = false;
	}

	pigX += glm::sin(pigDirection) * 1.0f * deltaTime;
	pigZ += glm::cos(pigDirection) * 1.0f * deltaTime;
	pigJumpProg += 5.0f * deltaTime;

	GLfloat frand();

	if (frand() < 0.005f)
	{
		pigDirection = frand() * 6.28f;
	}

	pigY = glm::abs(glm::sin(pigJumpProg)) * 0.2f;
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	
}

bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	//for Mac OS X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
//	glfwSetScrollCallback(glWindow, scrollCallback);
    //glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}

void initOpenGLState()
{
	glClearColor(0.5, 0.5, 0.5, 1.0);
	glViewport(0, 0, retina_width, retina_height);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

glm::mat4 computeLightSpaceTrMatrix()
{
	glm::mat4 lightProjection = glm::ortho(-55.0f, 55.0f, -55.0f, 55.0f, 1.0f, 150.0f);
	glm::mat4 lightView = glm::lookAt(glm::vec3(30.0f, 46.0f, 30.0f), glm::vec3(25.0f, 1.0f, 25.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	return lightProjection * lightView;
}

void initModels()
{
	ground = gps::Model3D("objects/ground/ground.obj1", "objects/ground/");
	objCasa = gps::Model3D("objects/obiecteCasa/obiecteCasaBun2.obj1", "objects/obiecteCasa/");
	casa = gps::Model3D("objects/casa/casaBun1.obj1", "objects/casa/");
	usa = gps::Model3D("objects/usa/usaBun.obj1", "objects/usa/");
	usaDeschisa = gps::Model3D("objects/usa/usaDesc.obj1", "objects/usa/");
	dovleac = gps::Model3D("objects/dovleac/Halloween_Pumpkin.obj1", "objects/dovleac/");
	copac = gps::Model3D("objects/copac/tree.obj1", "objects/copac/");
	porc = gps::Model3D("objects/porc/porc.obj1", "objects/porc/");

	//capac = gps::Model3D("objects/capacCufar.obj", "objects/");
	//capacRidicat = gps::Model3D("objects/capacRidicat.obj", "objects/");
}

void initShaders()
{
	myCustomShader.loadShader("shaders/shaderStart.vert", "shaders/shaderStart.frag");
	depthMapShader.loadShader("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag");
	//myCustomShader.useShaderProgram();
}

void initUniforms()
{
	myCustomShader.useShaderProgram();

	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");

	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");

	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");

	lightDirMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDirMatrix");

	projection = glm::perspective(glm::radians(fov), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 10.0f, 2.0f);
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	//set light color
	lightColor = glm::vec3(0.75f, 0.75f, 1.0f);
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	//set light pos
	lightPos = glm::vec3(20.0f, 0.1f, 30.0f);
	lightPosLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightPos");
	glUniform3fv(lightPosLoc, 1, glm::value_ptr(lightPos));

	isLightLoc = glGetUniformLocation(myCustomShader.shaderProgram, "isLight");
	isFogLoc = glGetUniformLocation(myCustomShader.shaderProgram, "isFog");
	glUniform1i(isLightLoc, isLight);
	glUniform1i(isFogLoc, isFog);
}

void initSkyBox()
{
	std::vector<const GLchar*> faces;
	faces.push_back("skybox/starfield_rt.tga");
	faces.push_back("skybox/starfield_lf.tga");
	faces.push_back("skybox/starfield_up.tga");
	faces.push_back("skybox/starfield_dn.tga");
	faces.push_back("skybox/starfield_bk.tga");
	faces.push_back("skybox/starfield_ft.tga");
	mySkybox.Load(faces);
	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE,
		glm::value_ptr(view));

	projection = glm::perspective(glm::radians(fov), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,
		glm::value_ptr(projection));
}

void initFBOs()
{
	//generate FBO ID
	glGenFramebuffers(1, &shadowMapFBO);

	//create depth texture for FBO
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//attach texture to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void renderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	myCustomShader.useShaderProgram();

	processMovement();

	depthMapShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(computeLightSpaceTrMatrix()));

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	for (int i = 0; i < 13; ++i)
	{
		for (int j = 0; j < 13; ++j)
		{
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3((i-2) * 6.0f, 0.0f, (j-2) * 6.0f));
			model = glm::scale(model, glm::vec3(3.0f, 1.0f, 3.0f));
			glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
			ground.Draw(depthMapShader);
		}
	}

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(20.0f, -0.8f, 50.0f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

	objCasa.Draw(depthMapShader);
	casa.Draw(depthMapShader);

	if (!usaDesc) {
		usa.Draw(depthMapShader);
	}
	else {
		usaDeschisa.Draw(depthMapShader);
	}

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(20.0f, 0.0f, 20.0f) + glm::vec3(pigX, pigY, pigZ));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	model = glm::rotate(model, pigDirection, glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	porc.Draw(depthMapShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(20.0f, 0.0f, 30.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	dovleac.Draw(depthMapShader);

	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 5; ++j)
		{
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(i * 12.5f, 0.0f, j * 12.5f) + treeOffsets[i][j]);
			glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
			copac.Draw(depthMapShader);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	myCustomShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "lightSpaceTrMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(computeLightSpaceTrMatrix()));

	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"),
		1,
		GL_FALSE,
		glm::value_ptr(view));

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "shadowMap"), 3);


	/*glm::mat4 lightProjection = glm::ortho(-30.0f, 30.0f, -30.0f, 30.0f, 1.0f, 50.0f);
	glm::mat4 lightView = glm::lookAt(glm::vec3(30.0f, 40.0f, 30.0f), glm::vec3(25.0f, 0.0f, 25.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"),
		1,
		GL_FALSE,
		glm::value_ptr(lightView));
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "projection"),
		1,
		GL_FALSE,
		glm::value_ptr(lightProjection));*/


	glViewport(0, 0, retina_width, retina_height);

	for (int i = 0; i < 12; ++i)
	{
		for (int j = 0; j < 12; ++j)
		{
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3((i-2) * 6.0f, 0.0f, (j-2) * 6.0f));
			model = glm::scale(model, glm::vec3(3.0f, 1.0f, 3.0f));
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
			glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(view * model));
			ground.Draw(myCustomShader);
		}
	}

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(20.0f, -0.8f, 50.0f));
	//model = glm::scale(model, glm::vec3(3.0f, 1.0f, 3.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(view * model));

	objCasa.Draw(myCustomShader);
	casa.Draw(myCustomShader);

	if (!usaDesc) {
		usa.Draw(myCustomShader);
	}
	else {
		usaDeschisa.Draw(myCustomShader);
	}

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(20.0f, 0.0f, 30.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(view * model));
	dovleac.Draw(myCustomShader);
	
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(20.0f, 0.0f, 20.0f) + glm::vec3(pigX, pigY, pigZ));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	model = glm::rotate(model, pigDirection, glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(view * model));
	porc.Draw(myCustomShader);

	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 5; ++j)
		{
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(i * 12.5f, 0.0f, j * 12.5f) + treeOffsets[i][j]);
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
			glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(view * model));
			copac.Draw(myCustomShader);
		}
	}

	if (!isFog)
		mySkybox.Draw(skyboxShader, view, projection);
}

GLfloat frand()
{
	return (GLfloat)rand() / RAND_MAX;
}

int main(int argc, const char * argv[]) {

	srand(time(NULL));

	initOpenGLWindow();
	initOpenGLState();
	initModels();
	initShaders();
	initUniforms();
	initFBOs();
	initSkyBox();

	for (int i = 0; i < 5; ++i)
		for (int j = 0; j < 5; ++j)
			treeOffsets[i][j] = glm::vec3((frand() - 0.5f) * 5.0f, 0.0f, (frand() - 0.5f) * 5.0f);
	
	glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();

		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	//close GL context and any other GLFW resources
	glfwTerminate();

	return 0;
}
